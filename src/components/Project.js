import React from 'react'
import Project1 from '../img/project1.png'
import { NavLink } from 'react-router-dom'


const Project = () => {
    return (
        <div className=' mt-20'>
            <div className=" container mx-auto font-DMSans">
                <div className=" block ">
                    <h1 className=' text-center text-xs font-bold text-[#AAAEAE] tracking-[8px]'>WORKS</h1>
                    <h1 className=' text-center mt-3 font-bold text-3xl text-[#434343] dark:text-[#D4D2D8]'>Featured <span className=' text-[#9E6AF3]'>Project</span></h1>
                </div>

                <div className=" pt-14 px-4 lg:px-24">
                    <div className=" grid grid-cols-1 sm:grid-cols-2 gap-5">
                        <div className="  w-full aspect-[3/4] md:aspect-video rounded-xl relative overflow-hidden group">
                            <div className="absolute flex-col flex justify-center items-center gap-7 inset-0 z-10 transition-opacity opacity-0 bg-gradient-to-bl from-[#473f63a5] to-[#0c0c20cc] dark:from-[#1d1a29a5] dark:to-[#010102cc] group-hover:opacity-100">
                                <h5 className=' text-[#fff] font-bold text-2xl text-center'>First Portfolio</h5>
                                <h6 className=' font-semibold text-[#fff]'>UI/UX Design</h6>
                                <button className=' font-bold border text-xs xl:text-sm bg-[#9E6AF3] border-[#9E6AF3] px-5 py-3 rounded-md text-[#fff] z-10 relative overflow-hidden before:absolute before:left-0 before:top-0 before:bottom-0 before:-z-10 before:h-full before:w-full before:origin-bottom before:scale-y-0 before:bg-[#fff] before:opacity-20 before:transition-transform before:duration-300 hover:text-[#fff] before:hover:scale-y-100 before:rounded-md'>View Project</button>
                            </div>
                            <img className=' h-full w-full object-cover scale-100 group-hover:scale-110 duration-500 ease-in-out' src={Project1} />
                        </div>
                        <div className="  w-full aspect-[3/4] md:aspect-video rounded-xl relative overflow-hidden group">
                            <div className="absolute flex-col flex justify-center items-center gap-7 inset-0 z-10 transition-opacity opacity-0 bg-gradient-to-bl from-[#473f63a5] to-[#0c0c20cc] dark:from-[#1d1a29a5] dark:to-[#010102cc] group-hover:opacity-100">
                                <h5 className=' text-[#fff] font-bold text-2xl text-center'>First Portfolio</h5>
                                <h6 className=' font-semibold text-[#fff]'>UI/UX Design</h6>
                                <button className=' font-bold border text-xs xl:text-sm bg-[#9E6AF3] border-[#9E6AF3] px-5 py-3 rounded-md text-[#fff] z-10 relative overflow-hidden before:absolute before:left-0 before:top-0 before:bottom-0 before:-z-10 before:h-full before:w-full before:origin-bottom before:scale-y-0 before:bg-[#fff] before:opacity-20 before:transition-transform before:duration-300 hover:text-[#fff] before:hover:scale-y-100 before:rounded-md'>View Project</button>
                            </div>
                            <img className=' h-full w-full object-cover scale-100 group-hover:scale-110 duration-500 ease-in-out' src={Project1} />
                        </div>
                        <div className="  w-full aspect-[3/4] md:aspect-video rounded-xl relative overflow-hidden group">
                            <div className="absolute flex-col flex justify-center items-center gap-7 inset-0 z-10 transition-opacity opacity-0 bg-gradient-to-bl from-[#473f63a5] to-[#0c0c20cc] dark:from-[#1d1a29a5] dark:to-[#010102cc] group-hover:opacity-100">
                                <h5 className=' text-[#fff] font-bold text-2xl text-center'>First Portfolio</h5>
                                <h6 className=' font-semibold text-[#fff]'>UI/UX Design</h6>
                                <button className=' font-bold border text-xs xl:text-sm bg-[#9E6AF3] border-[#9E6AF3] px-5 py-3 rounded-md text-[#fff] z-10 relative overflow-hidden before:absolute before:left-0 before:top-0 before:bottom-0 before:-z-10 before:h-full before:w-full before:origin-bottom before:scale-y-0 before:bg-[#fff] before:opacity-20 before:transition-transform before:duration-300 hover:text-[#fff] before:hover:scale-y-100 before:rounded-md'>View Project</button>
                            </div>
                            <img className=' h-full w-full object-cover scale-100 group-hover:scale-110 duration-500 ease-in-out' src={Project1} />
                        </div>
                        <div className="  w-full aspect-[3/4] md:aspect-video rounded-xl relative overflow-hidden group">
                            <div className="absolute flex-col flex justify-center items-center gap-7 inset-0 z-10 transition-opacity opacity-0 bg-gradient-to-bl from-[#473f63a5] to-[#0c0c20cc] dark:from-[#1d1a29a5] dark:to-[#010102cc] group-hover:opacity-100">
                                <h5 className=' text-[#fff] font-bold text-2xl text-center'>First Portfolio</h5>
                                <h6 className=' font-semibold text-[#fff]'>UI/UX Design</h6>
                                <button className=' font-bold border text-xs xl:text-sm bg-[#9E6AF3] border-[#9E6AF3] px-5 py-3 rounded-md text-[#fff] z-10 relative overflow-hidden before:absolute before:left-0 before:top-0 before:bottom-0 before:-z-10 before:h-full before:w-full before:origin-bottom before:scale-y-0 before:bg-[#fff] before:opacity-20 before:transition-transform before:duration-300 hover:text-[#fff] before:hover:scale-y-100 before:rounded-md'>View Project</button>
                            </div>
                            <img className=' h-full w-full object-cover scale-100 group-hover:scale-110 duration-500 ease-in-out' src={Project1} />
                        </div>
                    </div>
                </div>
                <NavLink to='/project'><div className=" flex justify-center pt-14 px-4 lg:px-24">
                    <button className=' font-bold border text-xs xl:text-sm bg-[#9E6AF3] border-[#9E6AF3] px-5 py-3 rounded-md text-[#fff] dark:text-[#181123] z-10 relative overflow-hidden before:absolute before:left-0 before:top-0 before:bottom-0 before:-z-10 before:h-full before:w-full before:origin-bottom before:scale-y-0 before:bg-[#fff] dark:before:bg-[#181123] before:transition-transform before:duration-300 dark:hover:text-[#9E6AF3] hover:text-[#9E6AF3] before:hover:scale-y-100 before:rounded-md'>View More {'>'}</button>
                </div></NavLink>
            </div>
        </div>
    )
}

export default Project