import React from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { useState } from 'react'

const Contoh = () => {
    let [isOpen, setIsOpen] = useState(false)

    function closeModal (){
        setIsOpen(false)
    }
    function openModal (){
        setIsOpen(true)
    }

    return (
        <div className=' flex justify-center mt-5'>
            <button className=' px-2 py-4 text-white bg-slate-900' onClick={openModal}> open modal</button>

            
            <Transition show={isOpen} >
                <Dialog className="relative z-[99999]" onClose={closeModal}>
                    <Transition.Child
                        enter="ease-out duration-1000"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                    <div className="fixed inset-0 bg-black bg-opacity-70 "/>
                    </Transition.Child>

                <div className="fixed inset-0 overflow-y-auto">
                    <div className="flex min-h-full items-center justify-center p-4 text-center">
                        <Transition.Child
                            enter="ease-out duration-300"
                            enterFrom="opacity-0 scale-95"
                            enterTo="opacity-100 scale-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100 scale-100"
                            leaveTo="opacity-0 scale-95"
                        >
                            <Dialog.Panel className=" w-full max-w-2xl transform overflow-hidden rounded-[20px] bg-white p-12 text-left align-middle transition-all sm:py-16 sm:px-20 opacity-100 scale-100">
                                <div className=" h-[30000px]"></div>
                            </Dialog.Panel>
                        </Transition.Child>
                    </div>
                </div>
                </Dialog>
            </Transition>
        </div>
    )
}

export default Contoh