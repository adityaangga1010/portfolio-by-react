import React from 'react'

const Index = () => {
    return (
        <div className='pt-20 md:pt-32 xl:pt-32 2xl:pt-36'>
            <div className=" container mx-auto">
                <div className=" px-4 lg:px-24">
                    <div className=" font-DMSans">
                        <h1 className=' text-3xl md:text-5xl md:leading-[70px] font-bold text-[#434343] dark:text-[#D4D2D8]'>The project that i made <span className=' block text-[#9E6AF3]'>with creativity</span></h1>
                    </div>
                    <div className=" pt-14  ">
                        <div className=" grid grid-cols-1 sm:grid-cols-2 gap-5">
                            <div className=" grid w-full aspect-[3/4] md:aspect-video bg-[#727877] opacity-20 rounded-xl">
                                <img src="" alt="" />
                            </div>
                            <div className=" grid w-full aspect-[3/4] md:aspect-video bg-[#727877] opacity-20 rounded-xl">
                                <img src="" alt="" />
                            </div>
                            <div className=" grid w-full aspect-[3/4] md:aspect-video bg-[#727877] opacity-20 rounded-xl">
                                <img src="" alt="" />
                            </div>
                            <div className=" grid w-full aspect-[3/4] md:aspect-video bg-[#727877] opacity-20 rounded-xl">
                                <img src="" alt="" /> 
                            </div>
                            <div className=" grid w-full aspect-[3/4] md:aspect-video bg-[#727877] opacity-20 rounded-xl">
                            <img src="" alt="" />
                            </div>
                            <div className=" grid w-full aspect-[3/4] md:aspect-video bg-[#727877] opacity-20 rounded-xl">
                                <img src="" alt="" /> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Index