import React from 'react'
import Main1 from '../components/Main1'
import Nav from '../components/Nav'
import Services from '../components/Services'
import Project from '../components/Project'
import Contact from '../components/Contact'
import Footer from '../components/Footer'
// gsap
import {gsap} from 'gsap'


const Home = () => {
    let tl = new gsap.timeline();
    return (
        <div className=' bg-[#fff] scrollbar scrollbar-hidden scroll-smooth dark:bg-[#181123] duration-700 ease-in-out'>
            <Nav timeline = {tl}/>
            <Main1 />
            <Services />
            <Project />
            <Contact />
            <Footer />
        </div>
    )
}

export default Home